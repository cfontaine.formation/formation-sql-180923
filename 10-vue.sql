USE bibliotheque;

-- Création d'une vue
CREATE VIEW v_auteur_livre AS
SELECT livres.id,titre,
concat(auteurs.prenom , ' ',auteurs.nom) AS auteur,
genres.nom AS genre_livre
FROM livres
INNER JOIN genres ON livres.genre=genres.id
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id
INNER JOIN auteurs ON  livre2auteur.id_auteur=auteurs.id;

-- La vue est considéré comme une tables par mariadb
SHOW tables;

-- On peut exécuter des requêtes sur la vue
SELECT * FROM v_auteur_livre; 

SELECT * FROM v_auteur_livre 
WHERE genre_livre ='Policier';

-- Si on ajoute des données dans la vue
INSERT INTO livres(titre,annee,genre) VALUES ('Les Misérables',1862,7);
INSERT INTO livre2auteur(id_livre,id_auteur)VALUES(143,33) ;

-- Elle mise à jour dynamiquement
SELECT * FROM v_auteur_livre ORDER BY id; 

-- ALTER VIEW -> Modifier une vue (recréation)
ALTER VIEW v_auteur_livre AS
SELECT livres.id,titre,annee,
concat(auteurs.prenom , ' ',auteurs.nom) AS auteur,
genres.nom AS genre_livre
FROM livres
INNER JOIN genres ON livres.genre=genres.id
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id
INNER JOIN auteurs ON  livre2auteur.id_auteur=auteurs.id;
-- \->la colonne annee est ajoutée à la table
SELECT * FROM v_auteur_livre ORDER BY id; 

-- DROP VIEW -> Supprimer une vue  
DROP VIEW v_auteur_livre;