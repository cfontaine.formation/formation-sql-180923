USE bibliotheque;
 
-- Jointure interne => INNER JOIN

-- Relation 1,n
-- Avec WHERE
-- Afficher le prenom, le nom de l'auteur et la nationnalité de l'auteur
SELECT prenom, auteurs.nom, pays.nom , nation FROM auteurs, pays
WHERE auteurs.nation = pays.id;

-- INNER JOIN
-- On va joindre la table pays et la table auteurs en utilisant l'égalité entre
-- La clé primaire de pays -> id et la clé étrangère d'auteurs -> nation

-- Afficher le prenom, le nom de l'auteur et la nationnalité de l'auteur
SELECT prenom,auteurs.nom nom,pays.nom pays FROM pays 
INNER JOIN auteurs ON pays.id=nation; 

-- Afficher le titre, l'année de sortie et le genre du livre
SELECT titre, genres.nom genre FROM genres
INNER JOIN livres ON genres.id=livres.genre ;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1960 et 1970
-- trié par année croissante
SELECT titre, annee, genres.nom genre FROM genres
INNER JOIN livres ON genres.id=livres.genre
WHERE annee BETWEEN 1960 AND 1970
ORDER BY annee;

-- Afficher le titre, l'année de sortie et le genre du livre pour les livres qui sont des livres policier ou fantastique 
-- trié par genre et par année croissante
SELECT titre, annee, genres.nom AS genre FROM genres
INNER JOIN livres ON genres.id=livres.genre
WHERE genres.nom IN('Policier','Fantastique')
ORDER BY genres.nom,annee;

-- Relation n,n
-- On va joindre : la table livres et la table de joiture livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
-- Afficher le titre du livre et le prénom et le nom de son auteurs
SELECT titre,prenom,nom FROM livres 
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id; 

-- Afficher le titre,le prenom, le nom et la nationalité pour les auteurs Français ou belge
SELECT titre,prenom,auteurs.nom,pays.nom  FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id
INNER JOIN pays ON auteurs.nation =Pays.id 
WHERE pays.nom IN('france','belgique');

-- Afficher le titre et le genre du livre et le prénom et le nom de son auteurs, pour les auteurs vivants
SELECT titre,genres.nom,auteurs.prenom,auteurs.nom FROM genres
INNER JOIN livres ON livres.genre=genres.id 
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON livre2auteur.id_auteur = auteurs.id
WHERE auteurs.deces IS NULL ORDER BY genres.nom ;

-- Exercice jointure interne
USE world;

-- Afficher chaque nom de pays et le nom de sa capitale
SELECT country.name, city.name FROM country
INNER JOIN city ON country.capital=city.id;

-- Afficher les nom de pays et le nom de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name, city.name FROM country
INNER JOIN city ON country.Code = city.CountryCode
ORDER BY country.name, city.name;

-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant
SELECT country.name, countrylanguage.language, countrylanguage.Percentage FROM country
INNER JOIN countrylanguage ON countrylanguage.CountryCode = country.Code 
WHERE countrylanguage.IsOfficial='T' 
ORDER BY country.name,countrylanguage.Percentage DESC;


-- Produit cartésien -> Cross Join
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.

USE exemple;
CREATE TABLE plats(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR (50)
);

CREATE TABLE boissons(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR (50)
);

INSERT INTO plats(nom)VALUES
('Céréale'),
('Pain'),
('oeuf sur le plat');

INSERT INTO boissons(nom)VALUES
('Café'),
('Thé'),
('Jus de fruit');

-- On obtient toutes les combinaisons possible entre les boissons et les plats
SELECT plats.nom AS plat,boissons.nom AS boisson FROM plats 
CROSS JOIN boissons;

USE bibliotheque ;

-- Jointure externe: LEFT JOIN ou RIGTH JOIN
SELECT nom, titre FROM genres 
LEFT JOIN livres ON genre=genres.id
WHERE titre IS NULL;

SELECT genres.nom, titre FROM livres
RIGHT JOIN genres ON livres.genre=genres.id; 

-- FULL JOIN
SELECT nom, titre FROM genres FULL JOIN livres ON livres.genre=genres.id;

-- Jointure naturelle (MySQL/ MariaDB)
-- Il faut que la clé primaire et la clé étrangère doivent avoir le même nom
ALTER TABLE genres CHANGE id genre INT;

SELECT titre, genres.nom FROM livres 
NATURAL JOIN genres;

ALTER TABLE genres CHANGE genre id INT;

-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)

USE exemple;

CREATE TABLE salaries(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR(50),
	manager INT,
	
	CONSTRAINT FK_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom,nom,manager) VALUES 
('John','Doe',NULL),
('Alan','Smithee',1),
('Jane','Doe',1),
('Jo','Dalton',NULL),
('yves','roulo',4);

SELECT employe.prenom employe_prenom, employe.nom employe_nom, manager.prenom manager_prenom,manager.nom manager_nom
FROM salaries AS employe 
LEFT JOIN salaries AS manager ON employe.manager=manager.id;

-- Exercice: Jointure interne et externe
USE world;

-- Afficher le nom des pays sans ville
SELECT country.name AS pays 
FROM country
LEFT JOIN city
ON country.code = city.countrycode
WHERE city.name IS NULL;

-- Afficher le  nom du pays, sa langue officielle (sans les pays qui n'ont pas de langue officielle)
SELECT country.name, countrylanguage.language 
FROM country
INNER JOIN countrylanguage 
ON country.code = countrylanguage.countrycode
WHERE countrylanguage.isOfficial = 'T'
ORDER BY country.name;

-- Afficher chaque ville et son pays
SELECT city.name ville,country.name pays
FROM country
INNER JOIN city
ON city.countrycode =country.code; 

-- Afficher tous les pays qui parlent français
SELECT country.name AS pays_francophone 
FROM country 
INNER JOIN countrylanguage
ON country.code =countrylanguage.countrycode
WHERE countryLanguage.language='French' AND countryLanguage.IsOfficial ='T';

USE elevage;
-- obtenir la liste des races de chiens qui sont des chiens de berger
SELECT race.nom FROM race
INNER JOIN espece ON race.espece_id = espece.id 
WHERE espece.nom_courant='Chien' AND race.nom LIKE 'berger%';

-- obtenir la liste des animaux (leur nom, date de naissance et race) pour lesquels nous n'avons aucune information sur leur pelage
SELECT animal.nom, date_naissance, race.nom FROM animal
INNER JOIN race ON animal.race_id = race.id
WHERE NOT(description LIKE '%pelage%' OR description LIKE '%robe%' OR description LIKE '%poil%')