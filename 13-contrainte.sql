USE exemple;

-- CHECK -> est utilisée pour limiter la plage de valeurs
CREATE TABLE personnes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40) NOT NULL,
	nom VARCHAR(40) NOT NULL,
	age INT CHECK(age>=0) 
);

INSERT INTO personnes(prenom,nom,age) VALUES ('john','doe',35);

-- Erreur -> la valeur pour age doit être > ou = à 0
INSERT INTO personnes(prenom,nom,age) VALUES ('jane','doe',-1);

DROP TABLE personnes;

CREATE TABLE personnes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40) NOT NULL,
	nom VARCHAR(40) NOT NULL,
	age INT ,
	CONSTRAINT chk_personne CHECK (age>=0 AND prenom LIKE 'b%')
);

INSERT INTO personnes(prenom,nom,age) VALUES ('Bastien','doe',35);

-- Erreur -> le prénom doit commencer par un b
INSERT INTO personnes(prenom,nom,age) VALUES ('John','doe',35);

-- Erreur -> l'age doit être > ou = à 0
INSERT INTO personnes(prenom,nom,age) VALUES ('Béatrice','doe',-1);

-- Cascade
USE exemple;

CREATE TABLE marque_cascades(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL
);

CREATE TABLE produits(
	id INT PRIMARY KEY AUTO_INCREMENT,
	description VARCHAR(80) NOT NULL,
	prix DECIMAL(6,2) NOT NULL,
	marque INT,
	CONSTRAINT FK_MARQUE_PRODUIT
	FOREIGN KEY(marque)
	REFERENCES marque_cascades(id) ON DELETE CASCADE ON UPDATE CASCADE;
);

INSERT INTO marque_cascades (nom) VALUES 
('Marque A'),
('Marque B'),
('Marque C');

INSERT INTO produits (description,prix,marque) VALUES 
('Souris',30.0,1),
('Clavier AZERTY',20.0,1),
('TV 4K',600.0,2),
('Carte mère ',130.0,3),
('Carte graphique',500.0,3);

SELECT * FROM marque_cascades;
SELECT * FROM produits;

-- ON DELETE CASCADE
DELETE FROM marque_cascades WHERE id=3; 
-- La suppression de la marque id=3 est propagée au article id=4 et id=5

SELECT * FROM marque_cascades;
SELECT * FROM produits;

-- ON UPDATE CASCADE
UPDATE marque_cascades SET id=5 WHERE id=2;
-- la mise à jour de la clé primaire id 2 à 5 est propagée
-- à la clé étrangère marque (2 à 5) de l'article id=3

SELECT * FROM marque_cascades;
SELECT * FROM produits;

ALTER TABLE produits DROP CONSTRAINT FK_MARQUE_PRODUIT;

ALTER TABLE produits ADD CONSTRAINT FK_MARQUE_PRODUIT
FOREIGN KEY(marque)
REFERENCES marque_cascades (id) ON DELETE SET NULL ON UPDATE SET NULL;

-- ON DELETE SET NULL
DELETE FROM marque_cascades WHERE id=1;
-- La suppression de la marque id=1 entraine la modification  à NULL des clés étrangères 
-- marque des articles id=1 et id=2

SELECT * FROM marque_cascades;
SELECT * FROM produits;

-- ON UPDATE SET NULL
UPDATE marque_cascades SET id=10 WHERE id=5;
-- la mise à jour de la clé primaire id 5 à 10
-- entraine la modification à NULL de la clé étrangère de l'article id=3
SELECT * FROM marque_cascades;
SELECT * FROM produits;

USE bibliotheque;

-- EXPLAIN devant SELECT permet d’afficher le plan d’exécution d’une requête SQL
-- cela permet de savoir comment la requête va être exécuter par le SGBD et de voir les index utilisés
EXPLAIN SELECT titre,annee, genres.nom, prenom,auteurs.nom,pays.nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur 
INNER JOIN livres ON livres.id = livre2auteur.id_livre
INNER JOIN genres ON livres.genre = genres.id
INNER JOIN pays ON nation=pays.id 
WHERE pays.nom IN ('belgique','france') AND genres.nom='Policier';

-- Créer un index sur la colonne annee de livres
CREATE INDEX INDEX_LIVRE_ANNEE ON livres(annee);

-- Supprimer un index
DROP INDEX INDEX_LIVRE_ANNEE ON livres;

