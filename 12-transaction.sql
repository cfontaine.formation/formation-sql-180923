USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(10,2),
	iban VARCHAR(20),
	prenom  VARCHAR(40),
	nom VARCHAR(40)
);

INSERT INTO compte_bancaire(solde,iban,prenom,nom) VALUES
(400.00,'5962-0000-0001','John','Doe'),
(1400.00,'5962-0000-0100','Jane','Doe'),
(400.00,'5962-0000-0012','Alan','Smithee'),
(400.00,'5962-0000-0031','Yes','Roulo');

-- desactiver l'autocommit
SET autocommit=0;
-- transfert entre 2 comptes bancaires
START TRANSACTION; -- Ouvertutre de la TRANSACTION
UPDATE compte_bancaire SET solde=solde-50.00 WHERE id=2;
UPDATE compte_bancaire SET solde=solde+50.00 WHERE id=4;
COMMIT; -- Validation de a TRANSACTION

SELECT * FROM compte_bancaire ;

START TRANSACTION; -- Ouvertutre de la TRANSACTION
UPDATE compte_bancaire SET solde=solde-500.00 WHERE id=2;
UPDATE compte_bancaire SET solde=solde+500.00 WHERE id=3;
ROLLBACK; -- Annuler la TRANSACTION

SELECT * FROM compte_bancaire ;

-- Point de sauvegarde
START TRANSACTION; -- Ouvertutre de la TRANSACTION
SAVEPOINT transfert1; 	-- Création d'un point de sauvegarde
UPDATE compte_bancaire SET solde=solde-100.00 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+100.00 WHERE id=3;
RELEASE SAVEPOINT transfert1; -- Retour au point de sauvegarde transfert1

SAVEPOINT creation1;
INSERT INTO compte_bancaire(solde,iban,prenom,nom) VALUES
(40.00,'5962-0000-0028','Jo','Dalton');
ROLLBACK TO creation1;	-- Retour au point de sauvegarde creation1

SAVEPOINT transfert2; 
UPDATE compte_bancaire SET solde=solde-10.00 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+10.00 WHERE id=2;

-- Libération des points de sauvegarde
RELEASE SAVEPOINT transfert1;
RELEASE SAVEPOINT creation1;
RELEASE SAVEPOINT transfert2;
COMMIT;

SELECT * FROM compte_bancaire ;

-- activation de l'autocommit
SET autocommit =1;