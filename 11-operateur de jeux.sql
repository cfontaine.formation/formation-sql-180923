USE exemple;

CREATE TABLE employees(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_personnel VARCHAR(255),
	salaire_mensuel double,
	temps_travail_semaine INT
);


CREATE TABLE clients(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	adresse_livraison VARCHAR(255),
	adresse_personnel VARCHAR(255)
);

INSERT INTO employees(prenom,nom, adresse_personnel,salaire_mensuel,temps_travail_semaine)
VALUES
('John','Doe','1,rue esquermoise Lille',1800,35),
('Jane','Doe','1,rue esquermoise Lille',2000,35),
('Jo','Dalton','46,rue des Cannoniers  Lille',2800,40),
('Alan','Smithee','32 Boulevard Vincent Gache Nantes',2300,37),
('Yves','Roulo','1,46,rue des Cannoniers  Lille',1400,20);

INSERT INTO Clients(prenom,nom,adresse_livraison , adresse_personnel)
VALUES
('John','Doe','1,rue esquermoise Lille','1,rue esquermoise Lille'),
('Pierre','Martin','23,rue esquermoise Lille','23,rue esquermoise Lille'),
('Bastien','Dupond','46,rue des Cannoniers  Lille','46,rue des Cannoniers  Lille');

-- Union -> UNION qui permet de concaténer les résultats de 2 requêtes ou plus
-- les requêtes à concaténer retournent:
--		- le même nombre de colonnes
-- 		- les mêmes types de données
--      - dans le même ordre
SELECT prenom, nom,adresse_personnel , 'emp' AS identifiant FROM employees
Union 
SELECT prenom,nom,adresse_personnel , 'cl' AS  identifiant FROM clients;

-- Pas de doublon avec union
SELECT prenom, nom,adresse_personnel FROM employees
Union 
SELECT prenom,nom,adresse_personnel FROM clients

-- Union ALL autorise les doublon
SELECT prenom, nom,adresse_personnel FROM employees
Union ALL
SELECT prenom,nom,adresse_personnel FROM clients;

-- Intersection
SELECT prenom, nom,adresse_personnel FROM employees
Intersect
SELECT prenom,nom,adresse_personnel FROM clients;

-- Except
SELECT prenom, nom,adresse_personnel FROM employees
Except
SELECT prenom,nom,adresse_personnel FROM clients;

-- Exercice: Opérateurs de jeux
USE world;
-- Afficher la liste des pays et des villes ayant moins de 1000 habitant
SELECT name , population , 'pays' FROM country WHERE population<1000 
UNION
SELECT name , population , 'ville' FROM city WHERE population<1000; 

-- Liste des pays où on parle français ou anglais
SELECT country.name as nom 
FROM countrylanguage
INNER JOIN country ON code = countrycode
WHERE Language = 'French'
UNION
SELECT country.name as nom 
FROM countrylanguage
INNER JOIN country ON code = countrycode
WHERE Language = 'English'
ORDER BY Nom;
