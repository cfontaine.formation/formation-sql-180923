-- Un commentaire fin de ligne en SQL
# Un commentaire fin de ligne en SQL

/*
 * Commentaire 
 * sur plusieurs
 * lignes
 */
show DATABASES;  -- Afficher l'ensemble des bases de données 

-- Créer un base de données exemple
CREATE DATABASE exemple;

-- Choisir la base de données exemple, comme base courante
USE exemple ;

-- Supprimer une base de donnée
DROP DATABASE exemple;

-- Créer une table article
CREATE TABLE article(
	reference INT,
	description VARCHAR(255),
	prix DOUBLE
);

-- Supprimer la table article
DROP TABLE article;

-- Lister les tables de la base de données
SHOW TABLES;

-- Afficher la description de la table
DESCRIBE article;

-- Exercice: créer la table vols
CREATE TABLE vols(
	numero_vol INT,
	heure_depart DATETIME,
	ville_depart VARCHAR(100),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(100)	
);

-- Modifier une table -> ALTER TABLE

-- renommer une table (mysql/mariadb)
RENAME TABLE article TO articles;

-- Ajouter une colonne
ALTER TABLE articles ADD nom VARCHAR(5);

-- Supprimer une colonne
ALTER TABLE articles DROP description;

-- Modifier le type d'une colonne	(VARCHAR(5) -> VARCHAR(60))
ALTER TABLE articles MODIFY nom VARCHAR(60);

-- Modifier le nom de la colonne (nom -> nom_article)
ALTER TABLE articles CHANGE nom nom_article VARCHAR(60);

-- Modifier une colonne pour ajouter une valeur par défaut 1.0 pour le prix de l'article
ALTER TABLE articles MODIFY prix DOUBLE DEFAULT 1.0;

-- Modifier une colonne pour ajouter NOT NULL au nom_article
ALTER TABLE articles MODIFY nom_article VARCHAR(60) NOT NULL;

-- Clé primaire
-- Une clé primaire est la donnée qui permet d'identifier de manière unique une ligne dans une table

-- Modifier la table articles pour que la colonne reférence deviennent la clé primaire de la table
ALTER TABLE articles ADD CONSTRAINT PK_ARTICLE PRIMARY KEY(reference);

-- Modifier la colonne référence pour ajouter AUTO_INCREMENT sur la clé primaire
ALTER TABLE articles MODIFY reference INT AUTO_INCREMENT;

-- Création de la table articles avec directement avec la clé primaire, NOT NULL ...  
DROP TABLE articles;
CREATE TABLE articles(
	reference INT PRIMARY KEY AUTO_INCREMENT , 
	description VARCHAR(255) NOT NULL, 
	nom VARCHAR(60) UNIQUE, -- UNIQUE -> Il ne peut pas avoir de doublon pour le nom du produit 
	prix DOUBLE	 NOT NULL DEFAULT 10 -- NOT NULL -> prix ne peut plus être égal à NULL, lorsque l'on ajoutera des données, on sera obligé de données un valeur à prix
									 -- DEFAULT -> permet de définir une valeur par défaut autre que NULL
);

-- Modifier la table vols pour que la colonne numero_vol deviennent la clé primaire de la table
ALTER TABLE vols ADD CONSTRAINT PK_VOLS PRIMARY KEY(numero_vol);

-- Exercice table pilotes
CREATE TABLE pilotes (
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50)  NOT NULL,
	nombre_heure_vol INT NOT NULL
);

-- si on n'a pas préciser NOT NULL ou AUTO_INCREMENT
ALTER TABLE pilotes MODIFY prenom VARCHAR(50) NOT NULL;
ALTER TABLE pilotes MODIFY numero_pilote INT AUTO_INCREMENT;

-- si on a oublié PRIMARY KEY
ALTER TABLE pilotes ADD CONSTRAINT PK_pilote PRIMARY KEY(numero_pilote);

-- Relation entre table
CREATE TABLE marques(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(80) NOT NULL,
	date_creation DATE
);

DROP TABLE articles ;
CREATE TABLE articles(
	reference INT PRIMARY KEY AUTO_INCREMENT , 
	description VARCHAR(255) NOT NULL, 
	nom VARCHAR(60) UNIQUE,
	prix DOUBLE	 NOT NULL DEFAULT 10,
	marque INT-- ,
	/*CONSTRAINT FK_articles_marques
	FOREIGN KEY (marque)
	REFERENCES marques(id)*/
);

-- Ajout d'une contrainte de clé étrangère 
-- clé étrangère colonne marque de article
-- elle fait référence à la clé primaire id de la table marques 
ALTER TABLE articles ADD 
CONSTRAINT FK_articles_marques 
FOREIGN KEY (marque)
REFERENCES marques(id);

-- Exercice: ajouter la relation entre vol et pilote
-- un vol à un pilote et un pilote peut être concerné par n vols

-- on ajoute une colonne dans vols
ALTER TABLE vols ADD pilote INT;

-- On ajoute la contrainte de clé primaire
ALTER TABLE vols ADD
CONSTRAINT FK_vols_pilotes
FOREIGN KEY (pilote)
REFERENCES pilotes(numero_pilote);


-- Relation n,n -> table de jointure
CREATE TABLE fournisseurs(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL
) 

-- Création de la table de jointure
CREATE TABLE articles_fournisseurs(
	id_article INT,
	id_fournisseur INT,
	
	CONSTRAINT FK_articles_fournisseurs
	FOREIGN KEY (id_article)
	REFERENCES articles(reference),
	
	CONSTRAINT FK_fournisseurs_articles
	FOREIGN KEY (id_fournisseur)
	REFERENCES fournisseurs(id),
	
	CONSTRAINT PK_articles_fournisseurs
	PRIMARY KEY(id_article,id_fournisseur)
);


-- Fin de l'écriture de la bdd de gestion de vol

-- Ajout d'un colonne à la table vols (clé étrangères vers la tables avions)
ALTER TABLE vols ADD id_avions INT;

-- Création de la table avions
CREATE TABLE avions (
	numero_avion INT PRIMARY KEY AUTO_INCREMENT,
	modele VARCHAR(60) NOT NULL,
	capacite INT NOT NULL
);

-- Ajout de clé étrangères entre vols et pilotes
ALTER TABLE vols ADD
CONSTRAINT FK_vols_avions
FOREIGN KEY (id_avions)
REFERENCES avions (numero_avion);

