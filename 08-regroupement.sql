USE bibliotheque;

-- Regroupement avec Group By
-- Obtenir le nombre d'auteur par pays
SELECT pays.nom, count(auteurs.id)FROM auteurs
INNER JOIN pays ON pays.id=auteurs.nation
GROUP BY pays.nom;

-- Le nombre de livre par auteur pour les auteurs qui ont écrit aumoins 3 livres
-- having -> condition (idem where) après le regroupement, Where se trouve toujours avant le regroupement
SELECT prenom,auteurs.nom, count(livres.id) AS nb_livre FROM auteurs
LEFT JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur 
LEFT JOIN livres ON livre2auteur.id_livre = livres.id 
GROUP BY auteurs.nom 
HAVING nb_livre>3
ORDER BY nb_livre DESC

-- Le nombre de livre par genre
SELECT genres.nom,count(livres.id)
FROM livres
INNER JOIN genres ON livres.genre=genres.id
GROUP BY genre;

-- Nombre de livre par genre pour le livres sortie après 1960 
-- pour les auteurs qui ont écrit au moins 10 livres
SELECT genres.nom,count(livres.id)
FROM livres
INNER JOIN genres ON livres.genre=genres.id
WHERE annee>1960
GROUP BY genre
HAVING count(livres.id)>10;

-- Exercice :Regroupement Group By
USE world;

--  Nombre de pays par continent
SELECT count(code), continent FROM country GROUP BY Continent ;

--  Afficher les continents et leur la population totale classé du plus peuplé au moins peuplé
SELECT continent, SUM(population) AS population_total FROM country GROUP BY Continent ORDER BY population_total DESC;

-- Nombre de langue officielle par pays - classé par nombre de langue officielle
SELECT name, count(CountryCode) AS nb_language FROM country
INNER JOIN countrylanguage ON code =CountryCode 
WHERE IsOfficial ='T'
GROUP BY name
ORDER BY nb_language DESC;

-- Fonction de fenetrage

USE exemple;

CREATE TABLE ventes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom_vendeur VARCHAR(30),
	annee INT,
	vente double
);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2020,10000),
('Patrick',2020,11000),
('Paola',2020,15000),
('Paul',2021,8000),
('Patrick',2021,10000),
('Paola',2021,5000),
('Paul',2022,14000),
('Patrick',2022,5000),
('Paola',2022,16000),
('Paul',2019,12000),
('Patrick',2019,10000),
('Paola',2019,4000),
('Paul',2018,8000),
('Patrick',2018,10000),
('Paola',2018,5000),
('Paul',2017,1000),
('Patrick',2017,5000),
('Paola',2017,6000);

USE exemple;
-- Fonction de fenetrage

-- Clause over
-- OVER() -> la partition est la table compléte
SELECT annee,nom_vendeur ,vente,SUM (vente) OVER() FROM ventes;

-- OVER(PARTITION BY annee) -> les partitions correspondent aux annees
SELECT annee,nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY annee) FROM ventes;

-- OVER(PARTITION BY nom_vendeur) -> les partitions correspondent aux nom_vendeur
SELECT annee, nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

-- Agregate window function
-- count, sum,min,max, avg
SELECT annee, nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

-- Ranking window fonction
-- ROW_NUMBER() -> numérote 1, 2, ... les lignes de la partitions
SELECT annee,nom_vendeur,vente ,ROW_NUMBER() OVER(PARTITION BY nom_vendeur ORDER BY vente,annee DESC) FROM ventes;

-- RANK() -> idem DENSE_RANK(), mais on aura des écarts dans la séquence des valeurs lorsque plusieurs lignes ont le même rang
-- 1 - 2 - 3 - 4 - 4 - 6
SELECT annee,nom_vendeur,vente ,RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- DENSE_RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY
-- 1 - 2 - 3 - 4 - 4 - 5
SELECT annee,nom_vendeur,vente ,DENSE_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- PERCENT_RANK() -> Calculer le rang en pourcentage
SELECT annee, nom_vendeur ,vente, PERCENT_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC )*100 FROM ventes;

-- NTILE(n)-> Distribue les lignes de chaque partition de fenêtre dans un nombre spécifié de groupes classés
SELECT annee,nom_vendeur,vente ,NTILE(5) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- Value window fonction 
-- LAG -> Renvoie la valeur de la ligne avant la ligne actuelle dans une partition
-- vente	lag(vente)
-- 16000
-- 15000	16000
SELECT annee,nom_vendeur,vente ,LAG(vente)OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- LEAD -> Renvoie la valeur de la ligne après la ligne actuelle dans une partition
-- vente	lead(vente)
-- 16000	5000
-- 5000		15000
SELECT annee,nom_vendeur,vente ,Lead(vente)OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- FIRST_VALUE ->Renvoie la valeur de l'expression spécifiée par rapport à la première ligne de la frame
SELECT annee,nom_vendeur,vente, FIRST_VALUE(vente)OVER(PARTITION BY nom_vendeur) FROM ventes;

-- LAST_VALUE -> Renvoie la valeur de l'expression spécifiée par rapport à la dernière ligne de la frame
SELECT annee,nom_vendeur,vente ,LAST_VALUE(vente)OVER(PARTITION BY nom_vendeur) FROM ventes;

-- NTH_VALUE -> Renvoie la valeur de l'argument de la nème ligne
SELECT annee,nom_vendeur,vente ,NTH_VALUE(vente,4) OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- CUME_DIST -> Calcule la distribution cumulée d'une valeur dans un ensemble de valeurs
SELECT annee,nom_vendeur,vente ,CUME_DIST() OVER(ORDER BY annee) FROM ventes;


-- Clause frame
SELECT annee, nom_vendeur ,vente,
sum(vente) OVER(ORDER BY annee
ROWS  BETWEEN  CURRENT ROW AND 4 FOLLOWING)
FROM ventes;

-- GROUP BY WITH ROLLUP
SELECT annee,SUM(vente) FROM ventes 
GROUP BY annee WITH ROLLUP ;

SELECT annee,SUM(vente),nom_vendeur FROM ventes 
GROUP BY annee,nom_vendeur WITH ROLLUP ;

SELECT pays.nom, count(auteurs.id)FROM auteurs
INNER JOIN pays ON pays.id=auteurs.nation
GROUP BY pays.nom WITH ROLLUP;

-- Exercice Regroupement (suite)
USE elevage;
-- Having
SELECT Race.nom, COUNT(Animal.race_id) AS nombre
FROM Race
LEFT JOIN Animal ON Animal.race_id = Race.id
GROUP BY Race.nom
HAVING nombre = 0;

-- With Rollup
SELECT Animal.sexe, Race.nom, COUNT(Animal.id) AS nombre
FROM Animal
INNER JOIN Race ON Animal.race_id = Race.id
GROUP BY Race.nom, sexe WITH ROLLUP;

-- Fonction de fenetrage
USE World;
SELECT 
Rank() OVER(PARTITION BY continent ORDER BY population DESC) AS classement,
Name, Continent, population ,
sum(population) OVER(PARTITION BY continent ) AS population_continent FROM country;
