USE bibliotheque;
-- Syntaxe d'une fonction
-- nom_fonction(param1,param2 ...)

-- Fonctions arithmétiques
-- CEIL -> 2, FLOOR -> 1, ROUND(1.462674,3) -> 1.463, TRUNCATE(1.4628474,3) -> 1,462
-- RAND() -> nombre aléatoire entre 0 et 1
SELECT CEIL(1.4) AS CEIL,floor(1.4) AS FLOOR,round(1.462674,3)AS ROUND
,TRUNCATE(1.4628474,3),rand();

-- On peut utiliser rand() pour sélectionner aléatoirement 5 livres 
SELECT titre FROM livres ORDER BY rand() LIMIT 5;


-- Fonction Chaine de caractère
-- Taille de la chaine de caractère nom en octets
SELECT nom , length(nom) AS nombre_octet FROM auteurs;

-- Nombre de caractère de la chaine de caractère nom 
-- On ne sélectionne que les lignes dont la nom fait plus de 6 caratères 
SELECT prenom,nom ,char_length(nom) AS nombre_caractere FROM auteurs WHERE char_length(nom)<6; 

-- Valeur en ASCII du caractère a et A -> 97 65
SELECT ascii('a'), ascii('A');

-- concat -> Concaténation de chaînes
SELECT concat (prenom,' ',auteurs.nom, ' (',pays.nom ,')') AS auteur
FROM auteurs
INNER JOIN pays ON pays.id=auteurs.nation ;

-- concat_ws -> Concaténation de chaînes avec un séparateur (->1er paramètre) 
-- prenom  nom  naissance
SELECT concat_ws (' ',prenom,nom,naissance) AS auteur FROM auteurs;

-- space(15) -> retourne une chaîne contenant 15 espaces
SELECT concat ('|',space(15),'|') 

-- insert: Insertion d'une chaîne à une position pos et pour num caractères
-- Insertion d'une chaîne ---- au 3 caractères et pour remplcer 2 caractères
-- James -> Ja----s
SELECT prenom, insert(prenom,3,0,'----') FROM auteurs;

-- remplace: Remplace toutes les occurrences d'une sous-chaîne par une nouvelle chaîne
-- remplace dans le nom er par ___
-- Hebert -> H__b__t
SELECT nom,replace(nom,'er','___') FROM auteurs;

-- repeat -> répéte le nom 4 fois
SELECT repeat(nom,4) FROM auteurs;

-- Inverse les caractères du nom
-- Ellroy -> yorllE
SELECT reverse(nom) FROM auteurs;

-- left(prenom,3) -> Extrait 3 caractère en partant de la gauche du prénom
-- right(prenom,2) -> Extrait 2 caractère en partant de la droite du prénom
SELECT prenom,left(prenom,3), right(prenom,2)  FROM auteurs;

--  substr -> Extraction d'une sous chaîne du prénom à partir du 2ème caractères et pour 3 caractères 
SELECT prenom,substr(prenom,2,3) FROM auteurs;

-- Renvoie la position de la première occurrence
-- de la chaîne 'o', dans la chaîne 'hello world' -> 5
SELECT position('o' IN 'hello world');

-- FIND_IN_SET Renvoie la position de la chaîne zse dans la chaîne 'aze,uio,zse,123,rty'
-- contenant une liste de sous-chaîne séparé par une virgule -> 3
SELECT FIND_IN_SET ('zse','aze,uiottt,zse,123,rty');

-- FIELD Renvoie la position de 'zse' dans la liste de valeur 'aze','uio','zse','123','rty'
SELECT FIELD ('zse','aze','zse','123','rty');

-- TRIM ->Retire les caractère blanc à droite et à gauche
SELECT TRIM('          hello world         '); -- donne 'hello world'
-- LTRIM -> Retire les caractère blanc à gauche de la chaîne
SELECT LTRIM(' Bonjour         '); # 'Bonjour         '  
-- RTRIM -> Retire les caractère blanc à droite de la chaîne
SELECT RTRIM(' Bonjour         '); # ' Bonjour'  

-- lower(prenom) -> Conversion en minuscule du prénom
-- upper(prenom) -> Conversion en majuscule du prénom
SELECT lower(prenom),upper(nom) FROM auteurs;

-- strcmp -> Compare 2 chaînes : 
-- 'hello' = 'hello' → 0 
-- 'bonjour' < 'hello' → -1
-- 'bonjour' > 'hello' → 1
SELECT  strcmp('bonjour','hello'), strcmp('hello','bonjour'),strcmp('hello','hello');

-- 3 -> 3 chiffres après la virgule
SELECT format(annee,3) FROM livres;


-- fonction temporelle
-- CURRENT_DATE() -> Date courante
-- CURRENT_TIME() -> Heure courante
-- CURRENT_TIMESTAMP() ou NOW() -> Date et heure courante
SELECT current_date(),current_time(),current_timestamp(),now();  

-- DATE -> Extrait la date de la Date et de l'heure courante -> 2023-09-20
-- Day -> Extrait le jour de la Date et de l'heure courante -> 20
SELECT DATE(current_timestamp()),Day(current_timestamp()) ;

-- dayname -> Le jour de la semaine d'une date et de l'heure courante -> Wednesday
-- MONTH -> Extrait le mois d'une date et de l'heure courante -> 9
-- monthname -> Le nom du mois d'une date et de l'heure courante -> September
SELECT dayname(current_timestamp()), MONTH(current_timestamp()),monthname (current_timestamp());

-- dayofweek -> Le numéro du jour de la semaine d'une date (1→ dimanche … 7→ samedi)
-- weekday -> Numéro de jour de la semaine (0 → lundi … 6 → dimanche)
SELECT dayofweek(current_timestamp()),weekday(current_timestamp()) ;

-- WEEK -> Numéro de semaine d'une date (0 à 53)
SELECT week(current_timestamp());

-- YEAR -> extraire l'année d'une date
-- quarter -> retourne le trimestre d'une date
SELECT YEAR('1967-10-10'),quarter(current_date()) ;

-- YEAR -> extraire l'année dde la date de naissance
SELECT prenom,nom,YEAR(naissance) FROM auteurs;

-- YEARWEEK -> Année et numéro de semaine d'une date (0 à 53)
SELECT YEARWEEK(current_date()), 
INSERT(YEARWEEK(current_date()),5,0,'_');

-- Nombre de jour entre 2 dates
SELECT DATEDIFF('2024-01-01',current_date());

-- ADDDATE -> Ajoute un intervalle à une date
-- SUBDATE -> Soustrait un intervalle à une date
SELECT adddate(current_date(),INTERVAL 2 month) ,subdate(current_date(),INTERVAL 3 year);

-- FROM_DAYS -> Convertie une valeur numérique en Date 
-- last_day -> Le dernier jour du mois (30,31,29,28)
SELECT FROM_DAYS(730669),last_day('2023-07-06') ;

-- DATE_FORMAT -> Formater une date
SELECT DATE_FORMAT(current_date(),'%e %m %Y');

-- STR_TO_DATE -> Convertir une chaîne en date suivant un format
SELECT STR_TO_DATE('10/05/2023','%d/%m/%Y');

-- L'heure
SELECT HOUR(current_time()),MINUTE(current_time()),SECOND(current_time()),microsecond(current_time()) ;
SELECT timediff('12:30:00',current_time()),subtime(current_time(),'05:00:00'),addtime(current_time(),'05:00:00');

SELECT time_to_sec(current_time()),SEC_TO_Time(40000);
SELECT TIME_FORMAT(current_time(),'%l:%i:%s %p');
 
-- Fonction d'agrégation
SELECT count(id) ,min(annee), max(annee),truncate(avg(annee),0) FROM livres;

-- Autre fonction 
SELECT current_user(), DATABASE(),VERSION(),LAST_INSERT_ID();

SELECT COALESCE(NULL,NULL,4,NULL,'az'); # 4
SELECT NULLIF('az','er'),NULLIF ('az','az');

SELECT annee,NULLIF(annee,1954) FROM livres;

-- Fonction IF
USE exemple;

CREATE TABLE comptes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	valeur DOUBLE,
	sens Char(1)
);

INSERT INTO comptes(valeur,sens)VALUES
(200,'-'),
(550,'+'),
(6,'+'),
(24000,'-');

SELECT IF(sens='-',-valeur,valeur) FROM comptes;

-- Fonction CASE
USE bibliotheque ;
SELECT annee,titre,
CASE 
	WHEN annee>2000 THEN 'Moderne'
	WHEN annee<=2000 AND annee>1900 THEN '20 eme siècle'
	ELSE 'Livre Ancien'
END AS type_livre
FROM livres;
END

SELECT genre, 
CASE(genre)
	WHEN 1 THEN 'Roman policier'
	WHEN 3 THEN 'Horreur'
	WHEN 7 THEN 'Drame'
	ELSE 'un autre genre'
END AS genre_livre
FROM livres;
END

-- Exercice Fonction
USE world;
-- Nombre de pays présents dans la table Country
SELECT count(country.code) FROM country;

-- Écrire une requête pour générer un code qui a pour forme les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par -
SELECT concat_ws('-',left(name ,3),'0000',char_length(name)) FROM city;  

USE elevage;
-- Afficher la liste des animaux nés en 2006
SELECT nom, date_naissance FROM animal WHERE YEAR(date_naissance)=2006; 

-- Afficher la liste des animaux nés au mois de mai en 2009
SELECT nom, date_naissance FROM animal WHERE YEAR(date_naissance)=2009 AND MONTH(date_naissance)=5; 

-- Afficher les chats dont la deuxième lettre du nom est un a
SELECT animal.nom, espece.nom_courant FROM animal 
INNER JOIN espece ON animal.espece_id=espece.id 
WHERE espece.nom_courant = 'chat' AND substr(nom,2,1)='a';
