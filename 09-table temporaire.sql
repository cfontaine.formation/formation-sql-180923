USE exemple;
-- CREATE TEMPORARY TABLE -> table temporaire
-- Elle sera supprimée dés que l'on quitte la session
CREATE TEMPORARY TABLE utilisateurs
( 
	id INT PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(200) NOT NULL,
	password VARCHAR(40) NOT NULL
);

INSERT INTO utilisateurs(email,password)VALUES
('jd@dawan.fr','dawan'),
('mdupont@dawan.fr','dawan123'),
('mr@dawan.fr','azerty');

-- affiche tous les utilisateur
SELECT * FROM utilisateurs;

-- On se deconnecte du SGBD, la table n'éxiste plus
SELECT * FROM utilisateurs;

-- Table CTE
-- est une requête utilisée en tant que table à usage unique
USE bibliotheque;
WITH auteur_vivant_cte AS
(
	SELECT auteurs.id AS id_auteur,prenom,Auteurs.nom as auteur_nom,naissance,pays.nom AS nationalite  FROM auteurs 
	INNER JOIN pays ON pays.id=auteurs.nation 
	WHERE deces IS NULL
	),
auteur_vivant_usa_cte AS -- On peut déclarer plusieurs CTE à la suite avant de les utiliser
(
	SELECT id_auteur,prenom,auteur_nom,naissance FROM auteur_vivant_cte WHERE nationalite='france'
)
-- La table CTE ne pourra être utilisée qu’immédiatement après sa déclaration
SELECT prenom ,auteur_nom,naissance FROM auteur_vivant_usa_cte WHERE naissance >'1910-01-01';
-- SELECT * FROM auteur_vivant_usa_cte; -- erreur

-- Création d'une table depuis une sélection de données
CREATE TABLE bakcup_livres SELECT * FROM livres;
DROP TABLE bakcup_livres ;

CREATE TEMPORARY TABLE bakcup_livres SELECT * FROM livres;
SELECT * FROM bakcup_livres ;