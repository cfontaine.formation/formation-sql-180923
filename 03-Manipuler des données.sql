/* 03-manipulation de données*/

USE exemple;

-- Ajouter des données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES(1,'Marque A','1984-03-12');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO marques(nom,date_creation)VALUES ('Marque B','2000-04-01');
INSERT INTO marques(nom)VALUES ('Marque B');

-- INSERER Plusieurs lignes
INSERT INTO articles(description,nom,prix,marque) VALUES 
('TV 4K','TV 4K',600.0,3),
('Disque SSD','Disque SSD 1Go',99.0,1),
('Souris','Souris sans fils',30.0,1);

---- Erreur => La marque avec l'id 20 n'existe pas, la contrainte d'intégritée référentielle, n'est pas respecté
-- INSERT INTO articles(description,nom,prix,marque) VALUES ('TV 4K','TV 4K100hz',600.0,20);

-- Insérer des données dans une relation n-n
INSERT INTO fournisseurs(nom) VALUES 
('Fournisseur 1'),
('Fournisseur 2');

-- Supprimer toutes les lignes de la 
INSERT INTO articles_fournisseurs(id_article,id_fournisseur) VALUES 
(1,1),
(1,2),
(3,2);

INSERT INTO articles(description,nom,marque) VALUES 
('carte mère','carte mère amd',3); 

-- Supprimer des données => DELETE, TRUNCATE
-- Supression de la ligne qui a pour id 2 dans la table marques
DELETE FROM marques WHERE id=2;

-- Supprimer toutes les lignes de la table articles_fournisseurs
DELETE FROM articles_fournisseurs ;

-- Supprimer toutes les lignes de la table qui ont un prix >100.0
DELETE FROM articles WHERE prix >100.0;

-- Valeur auto_increment
-- DELETE => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM articles;
INSERT INTO articles(description,nom,prix,marque) VALUES 
('TV 4K','TV 4K',600.0,3); -- id=7

-- TRUNCATE => réinitailise l'auto incrément
SET FOREIGN_KEY_CHECKS =0;	-- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table articles  => remet la colonne AUTO_INCREMENT à 0
TRUNCATE articles;
SET FOREIGN_KEY_CHECKS =1; --réactiver la vérification des clés étrangères

INSERT INTO articles(description,nom,prix,marque) VALUES 
('TV 4K','TV 4K',600.0,3); -- id=1

-- Modification des données => UPDATE
-- Modification de la description de l'article qui a pour reference 1
UPDATE articles SET prix=550.99 WHERE reference=1;
UPDATE articles SET nom='Disque SDD 2go', prix=130.0 WHERE reference=2;

-- Modification des article dont le prix est inférieur à 50 -> 20
UPDATE articles SET prix=20.0 WHERE prix<50;

-- Modifier tous les prix supérieur à 30 en les augmentant de 10%
UPDATE articles SET prix=prix*1.10 WHERE prix>30.0;

-- Pas de condition tous les prix passe à 1.0
UPDATE articles SET prix=1.0;

-- chargement de fichier de binaire => LOAD_FILE
USE pizzeria;
INSERT INTO pizzas(nom,base,photo) VALUES('pizza 4 fromages','rouge',LOAD_FILE('c:/Dawan/Formations/sql/pizza.jpg'));