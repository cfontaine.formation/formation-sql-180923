-- Création de la base de données
CREATE DATABASE pizzeria;

-- Rendre la base de donnée pizzeria courante
USE pizzeria;

-- Créer la table pizzas
CREATE TABLE pizzas(
	numero_pizza INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(70) NOT NULL,
	base VARCHAR(15) NOT NULL,
	prix DOUBLE NOT NULL DEFAULT 12.0,
	photo BLOB
);

-- Créer la TABLE ingredients
CREATE TABLE ingredients (
	numero_ingredient INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL DEFAULT ''
);

-- Créer la relation entre pizzas et ingrédients
-- relation n,n -> table de jointure
CREATE TABLE pizzas_ingredients(
	-- clé primaire composée 
	numero_pizza INT, 		-- clé étrangère vers la table pizzas
	numero_ingredient INT, 	-- clé étrangère vers la table ingrédients
	
	CONSTRAINT FK_pizzas_ingredients
	FOREIGN KEY (numero_pizza)
	REFERENCES pizzas(numero_pizza),
	
	CONSTRAINT FK_ingredients_pizzas
	FOREIGN KEY (numero_ingredient)
	REFERENCES ingredients(numero_ingredient),
	
	CONSTRAINT PK_pizzas_ingredients
	PRIMARY KEY (numero_ingredient,numero_pizza)
);

-- Suite: création des autre tables du MCD

CREATE TABLE livreurs(
	numero_livreur INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL,
	telephone CHAR(10) NOT NULL
);

CREATE TABLE clients(
	numero_client INT AUTO_INCREMENT PRIMARY KEY ,
	nom VARCHAR(50) NOT NULL,
	adresse CHAR(255) NOT NULL
);

CREATE TABLE commandes(
	numero_commande INT PRIMARY KEY AUTO_INCREMENT,
	heure_commande DATETIME NOT NULL,
	heure_livraison DATETIME NOT NULL,
	livreur INT,
	client INT,
	
	CONSTRAINT fk_commandes_livreurs
	FOREIGN  KEY (livreur)
	REFERENCES livreurs(numero_livreur),
	
	CONSTRAINT fk_commandes_clients
	FOREIGN  KEY (client)
	REFERENCES clients(numero_client)
);

-- Table de jonction entre pizza et commande
CREATE TABLE pizzas_commandes(
	num_pizza INT,
	num_commande INT,
	quantite INT NOT NULL DEFAULT 1,
	
	CONSTRAINT fk_pizzas_commandes
	FOREIGN KEY (num_pizza)
	REFERENCES pizzas(numero_pizza),
	
	CONSTRAINT fk_commandes_pizzas
	FOREIGN KEY (num_commande)
	REFERENCES commandes(numero_commande),
	
	CONSTRAINT PRIMARY KEY (num_pizza,num_commande)
);

