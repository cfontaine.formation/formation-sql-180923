USE bibliotheque;

-- Requête imbriqué une retourne qu'un seul résultat
-- tous les sortie la moyenne des année de sortie des livres +10
SELECT titre, annee FROM livres
WHERE annee=(SELECT round(AVG(annee),0)+10 FROM livres LIMIT 1 ) ;

-- Requête imbriqué une retourne qu'un plusieurs résultats -> IN
SELECT titre, annee FROM livres
WHERE annee IN (
	SELECT annee FROM livres
	INNER JOIN genres ON livres.genre=genres.id 
	WHERE genres.nom='Horreur')
ORDER BY annee;


-- Exists
SELECT genres.id, genres.nom FROM genres WHERE 
EXISTS (
		SELECT livres.id FROM livres
		WHERE livres.genre=genres.id);

SELECT genres.id, genres.nom FROM genres WHERE 
NOT EXISTS (
		SELECT livres.id FROM livres
		WHERE livres.genre=genres.id);

-- ALL
SELECT titre, annee FROM livres
WHERE annee!= ALL( -- != ou <> différent
	SELECT annee FROM livres
	INNER JOIN genres ON livres.genre=genres.id 
	WHERE genres.nom='Science-fiction');
)

-- ANY ou SOME
SELECT titre, annee FROM livres
WHERE annee= ANY(
	SELECT annee FROM livres
	INNER JOIN genres ON livres.genre=genres.id 
	WHERE genres.nom='Science-fiction');
)

-- Exercise
USE  Elevage;

SELECT Animal.nom FROM Animal
	WHERE espece_id = ( 
	 SELECT id FROM Espece
	 WHERE nom_courant LIKE 'tor%'
);

-- Afficher le nom de la race pour lequel on a des animaux dans l'élevage
SELECT Race.id,Race.nom FROM Race
WHERE EXISTS (
		SELECT id FROM Animal 
		WHERE Animal.race_id = Race.id);
